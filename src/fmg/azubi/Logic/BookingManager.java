package fmg.azubi.Logic;

import fmg.azubi.Flight;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class BookingManager {

	private static BookingManager instance;

	private static final int maxBookingID = 10000;
	private ArrayList<Integer> bookingIDs = new ArrayList<>();

	private HashMap<Flight, HashMap<Integer, String>> bookings;

	List idList = new ArrayList<>();

	/**
	 * In dieser Methode soll eine eindeutige ID zurückgeben oder -1 falls alle IDs
	 * vergeben sind Erstelle mithilfe der Klasse random eine zufällige Zahl
	 * zwischen 0 und der maxBookingID Diese ID muss einzigartig sein, also muss
	 * überprüft werden ob die ID bereits in der Liste bookingIDs existiert Falls
	 * die ID bereits existiert soll erneut versucht werden eine zufällige ID zu
	 * erstellen Beträgt die Anzahl der Versuche gleich maxBookingID, kann davon
	 * ausgegangen werden, dass alle IDs vergeben sind
	 */

	public int getUniqueBookingID() {

		Random bookingID = new Random();
		int bid = bookingID.nextInt(maxBookingID);
		if (!idList.contains(bid)) {

			idList.add(bid);
		} else {
			while (idList.contains(bid)) {

				bid = bid + 1;

			}
			idList.add(bid);
		}
		return bid;
	}

	int Flighticon;

	public String bookingExist(Flight flight, int bookingID) {
		if (bookings.containsKey(flight)) {
			if (bookings.get(flight).containsKey(bookingID)) {
				return bookings.get(flight).get(bookingID);
			}

		}

		return null;
	}

	/**
	 * Diese Methode soll einen Flug zu einem Passagier zuweisen und die Buchungs ID
	 * zurückgeben Es soll mit der global deklarierten HashMap bookings gearbeitet
	 * werden Diese enthält zu den verschiedenen Flügen jeweils eine weitere
	 * HashMap mit den Buchungen der Passagiere Gibt es zu einem Flug bereits
	 * Buchungen, kann dessen HashMap ausgelesen und weitere Einträge hinzugefügt
	 * werden Existieren noch keine Buchungen zu einem Flug, muss für diesen eine
	 * neue HashMap angelegt werden In jedem Falle muss eine Buchung, aus dem
	 * Passagiernamen und einer eindeutigen ID, in bookings angelegt werden
	 */
	public int bookFlight(Flight flight, String passengerName) {
		int i = getUniqueBookingID();
		if (bookings.containsKey(flight)) {
			bookings.get(flight).put(i, passengerName);
		} else {
			HashMap<Integer, String> idName = new HashMap();
			bookings.put(flight, idName);
			bookings.get(flight).put(i, passengerName);
		}

		return i;
	}

	/**
	 * Überprüfe ob in bookings der Flug existiert auf dem storniert werden soll
	 * Falls ja, entferne die zu stornierende Buchung
	 */
	public void cancelFlight(Flight flight, int bookingNumber) {
		bookings.get(flight).remove(bookingNumber);

	}

	private BookingManager() {
		bookings = new HashMap();
	}

	public static BookingManager getInstance() {
		if (instance == null) {
			instance = new BookingManager();
		}
		return instance;
	}

	public HashMap<Integer, String> getBookings(Flight flight) {
		return bookings.get(flight);
	}
}
