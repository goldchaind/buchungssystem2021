package fmg.azubi.Logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

public class IATA_Converter {

	private HashMap<String, String> iataMap;

	private static IATA_Converter instance;

	// getInstance() so lassen?
	public static IATA_Converter getInstance() {
		if (instance == null) {
			instance = new IATA_Converter();
		}
		return instance;
	}

	// Konstruktor so lassen?
	private IATA_Converter() {
		iataMap = new HashMap<>();
		loadTSV();
	}

	/**
	 * Fülle die iataMap mit den IATA-Codes als Keys und den Flughäfen als Values
	 * Die Daten sind in iata.tsv gespeichert und mit Tabstopps getrennt
	 */

	String path = "./iata.tsv"; // �nderung Anfang

	private void loadTSV(){
    	File iatacode = new File(path);
    	BufferedReader linereader = null;
    try {
    linereader  = new BufferedReader(new FileReader(path));
    String line;
    while (null != (line = linereader.readLine())) {
    	String[] iatalist = line.split("\t");
       
            String k�rzel = iatalist[0];
            String namekomplett = iatalist[1];
            if(iataMap.containsKey(k�rzel)== false) {
            	iataMap.put(k�rzel, namekomplett);
            }
            
           
        }
   
    }catch(

	Exception ex)
	{
		ex.printStackTrace();
		System.out.println("Error! Es ist ein Fehler aufgetreten.");
	} 
	finally
	{
		if (null != linereader) {
			try {
				linereader.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}
	}

	/**
	 * Gebe den zum IATA-Code zugehörigen Flughafen zurück
	 */
	public String getIATA_Name(String iataCode) {
		System.out.println(iataMap.get(iataCode));
		return iataMap.get(iataCode);
		// �nderung Ende
	}
}
