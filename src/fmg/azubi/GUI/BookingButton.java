package fmg.azubi.GUI;

import fmg.azubi.Flight;
import fmg.azubi.Logic.BookingManager;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import static javax.swing.JOptionPane.showMessageDialog;

public class BookingButton extends JButton implements ActionListener {

	private final static String statusCheck = "Check";
	private final static String statusBook = "Buchen";
	private final static String statusCancel = "Stornieren";

	public BookingButton() {
		setText("Check");
		addActionListener(this);
	}

	public void reset() {
		setText(statusCheck);
	}

	/**
	 * Diese Methode wird aufgerufen, wenn der Button geklickt wird. Je nach Status,
	 * soll eine andere Aktion ausgeführt werden. Check, überprüft ob die Eingabe
	 * aus der GUI einer Zahl oder einem Text entspricht. Handelt es sich um eine
	 * Zahl, soll überprüft werden, ob es eine Buchung zu dieser Nummer gibt. In
	 * diesem Fall soll in den Status Cancel gewechselt werden, ansonsten soll in
	 * den Status Book gewechselt werden Im Status Book, soll der Name der in der
	 * Eingabe steht als neue Buchung gespeichert werden Im Status Cancel soll die
	 * Buchung storniert werden, also aus dem System gelöscht
	 */
	@Override
	public void actionPerformed(ActionEvent e) {

		String input = GUI.getInstance().getBookingInput();
		boolean string = false;

		try {
			int inputInt = Integer.parseInt(input);

			Flight flight = GUI.getInstance().getBookingFlight();
			if (BookingManager.getInstance().bookingExist(flight, inputInt) != null) {
				if (getText().equals(statusCancel)) {
					BookingManager.getInstance().cancelFlight(flight, inputInt);
					showMessageDialog(null, "Flug wurde storniert");
					GUI.getInstance().updateBookings(flight);
					GUI.getInstance().clearTextfeld();
					setText(statusCheck);
				} else {
					showMessageDialog(null, "Buchung wurde gefunden");
					setText(statusCancel);
				}
			} else {
				showMessageDialog(null, "Diese Buchungsnummer existiert nicht");
			}

		} catch (NumberFormatException exception) {

			Flight flight = GUI.getInstance().getBookingFlight();
			System.out.println(getText().equals(statusBook));
			string = true;
			if (getText().equals(statusBook)) {
				int bookingID = BookingManager.getInstance().bookFlight(flight, input);
				showMessageDialog(null, "Flug wurde gebucht\nIhre Buchungsnummer lautet: " + bookingID);
				GUI.getInstance().clearTextfeld();
				GUI.getInstance().updateBookings(flight);
				setText(statusCheck);
			} else {

				setText(statusBook);
			}
		}
	}
	

}
