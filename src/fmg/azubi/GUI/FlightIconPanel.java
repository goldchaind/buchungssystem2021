package fmg.azubi.GUI;

import fmg.azubi.Flight;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

public class FlightIconPanel extends JPanel {

	ArrayList<FlightIcon> iconList;

	public FlightIconPanel() {
		setBounds(0, 100, 1500, 400);
	}

	/**
	 * Gibt die iconList zurück - falls diese "null" ist, soll sie zuerst
	 * initialisiert werden
	 */
	public ArrayList<FlightIcon> getIconList() {
		if (iconList == null) {
			iconList = new ArrayList<FlightIcon>();
		}
		return iconList;
	}

	/**
	 * Erstelle den Setter für die iconList
	 */
	public void setIconList(ArrayList<FlightIcon> flightIcons) {
		this.iconList = flightIcons;

	}

	/**
	 * Entferne alle vorhandenen Icons und zeichne diese neu Die zu zeichnenden
	 * Icons sind in iconList gespeichert "Neu zeichnen" bedeutet die Position der
	 * Icons zu setzen und zum Panel hinzuzufügen Mit updateUI() kann das Panel
	 * aktualisert werden
	 */
	public void update() {
		removeAll();
		for (int i = 0; i < iconList.size(); i++) {
			add(iconList.get(i));
		}

		updateUI();
	}

	/**
	 * Setze die IconList zurück und füge anschließend jeden Flug, in der
	 * mitgegebenen Liste flights, zum Panel hinzu Aktualisiere das Panel mithilfe
	 * einer internen Methode
	 */
	public void setFlights(ArrayList<Flight> flights) {
		getIconList().clear();
		for (int i = 0; i < flights.size(); i++) {
			addFlight(flights.get(i));
		}
		update();
	}

	/**
	 * Erstellt aus dem Flug ein FlightIcon. ImagePath ist dabei
	 * SizedImages\{airline Name}.jpg Wenn auf das Icon geklickt wird, soll die GUI
	 * den Flug der dahinter steckt markieren Nur falls ein Bild existiert, soll das
	 * Icon auch zur Liste hinzugefügt werden
	 */
	public void addFlight(Flight flight) {
		FlightIcon Icon = new FlightIcon("SizedImages\\" + flight.getAirline() + ".jpg", flight);
		 Icon.addMouseListener(new MouseAdapter() {
	            @Override
	            public void mouseClicked(MouseEvent e) {
	                super.mouseClicked(e);
	                GUI.getInstance().setFlightForBooking(flight);
	            }
	        });
		if (Icon.getImagePath() == null) {

		} else {
			getIconList().add(Icon);
		}

	}

}
