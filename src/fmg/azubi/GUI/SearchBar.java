package fmg.azubi.GUI;

import fmg.azubi.Flight;

import fmg.azubi.Logic.IATA_Converter;

import javax.swing.*;

import java.security.cert.CertPathValidatorException.BasicReason;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;

public class SearchBar extends JTextField {

	public static final String flightNumber = "Linie";
	public static final String destinationAirport = "Ziel Flughafen";
	public static final String departTime = "Abflugszeit";

	ArrayList<Flight> flights;

	public SearchBar(ArrayList<Flight> flights) {
		this.flights = flights;
	}

	/**
	 * Die Methode soll alle Ergebnisse zurückgeben, welche mit dem Text in
	 * Textfeld übereinstimmen Falls die Suchleiste leer ist, kann direkt eine
	 * leere Liste mit Ergebnissen zurückgegeben werden Überprüfe für jeden Flug
	 * ob der Suchtext mit dem Flugtext von Flight.toString() übereinstimmt
	 * Verwende den IATA-Converter um auch die IATA-Codes mit in die Suche
	 * einzubeziehen
	 */

	public ArrayList<Flight> getSearchResults() {

		String input = getText();

		if (input.equals("")) {
			return flights;
		} else if (IATA_Converter.getInstance().getIATA_Name(input) == null) {
			ArrayList<Flight> sorted_flights = new ArrayList<Flight>();
			for (int i = 0; i < flights.size(); i++) {
				if (flights.get(i).toString().contains(input)) {
					sorted_flights.add(flights.get(i));
				}
			}
			return sorted_flights;
		} else {
			ArrayList<Flight> sorted_flights = new ArrayList<Flight>();
			for (int i = 0; i < flights.size(); i++) {
				if (flights.get(i).toString().contains(IATA_Converter.getInstance().getIATA_Name(input))) {
					sorted_flights.add(flights.get(i));
				}
			}
			return sorted_flights;
		}
	}

	/**
	 * Diese Methode soll eine Liste mit Flügen ausgeben, welche nach einem
	 * Kriterium sortiert sind Suche mithilfe von getSearchResults() nach
	 * übereinstimmenden Flügen Sortiere diese nach dem ausgewählten Kriterium
	 * (Tipp: Suche im Internet nach List.sort und Comparator)
	 */
	public ArrayList<Flight> getSearchResults(String sortingAttribute) {

		ArrayList<Flight> filtered_flights = getSearchResults();
		
		Collections.sort(filtered_flights, new Comparator<Flight>() {

			@Override
			public int compare(Flight o1, Flight o2) {
				
				if (sortingAttribute.equals(flightNumber)) {
					return o1.getFlightNumber().compareTo(o2.getFlightNumber());
				} else if (sortingAttribute.equals(destinationAirport)) {
					return o1.getDestination().compareTo(o2.getDestination());
				} else {
					return o1.getStartTime().compareTo(o2.getStartTime());
				}
			}
		});

		return filtered_flights;
	}

}
