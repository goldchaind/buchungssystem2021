package fmg.azubi.GUI;

import fmg.azubi.Database.DatabaseConnection;
import fmg.azubi.Logic.BookingManager;
import fmg.azubi.Flight;

import javax.swing.*;
import javax.swing.text.AttributeSet.ColorAttribute;

import java.awt.Color;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.HashMap;

public class GUI extends JFrame {

    private int width = 1500;
    private int height = 800;

    private int searchBarWidth = 400;
    private int searchBarHeight = 27;

    private String[] attributes = {SearchBar.departTime,SearchBar.flightNumber,SearchBar.destinationAirport};

    private SearchBar searchBar;
    JComboBox<String> sortingAttributeBox;
    FlightIconPanel iconPanel;
    JButton searchButton;
    JScrollPane pane;
    JLabel passengerNameHint;
    JTextField passengerName;
    BookingButton bookingActionButton;
    JTextArea tfName;

    FlightIcon flightIconForBooking = null;

    private static GUI instance;

    public static GUI getInstance(){
        return instance;
    }

    public GUI(){
        super();
        instance = this;
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setBounds(0,0,width,height);
        
        FileInputStream fis;

        
        try {
            searchBar = new SearchBar(DatabaseConnection.getInstance().getAllFlights());
        }catch (Exception ex){
        	searchBar = new SearchBar(new ArrayList<>());
            ex.printStackTrace();
        }
        searchBar.setBounds((width-searchBarWidth)/2,0,searchBarWidth,searchBarHeight);
        add(searchBar);


        sortingAttributeBox = new JComboBox<>(attributes);
        sortingAttributeBox.setBounds((width-searchBarWidth)/2+searchBarWidth+100,0,150,27);
        sortingAttributeBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                iconPanel.setFlights(searchBar.getSearchResults((String)sortingAttributeBox.getSelectedItem()));
            }
        });
        add(sortingAttributeBox);

        iconPanel = new FlightIconPanel();
        iconPanel.setFlights(searchBar.getSearchResults((String)sortingAttributeBox.getSelectedItem()));


        searchButton = new JButton("Suchen");
        searchButton.setBounds((width-searchBarWidth)/2+searchBarWidth,0,100,27);
        searchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                iconPanel.setFlights(searchBar.getSearchResults((String)sortingAttributeBox.getSelectedItem()));

            }
        });
        add(searchButton);

        pane = new JScrollPane(iconPanel);
        pane.setBounds(0,searchBarHeight,1480,300);
        add(pane);

        setLayout(null);
        setVisible(true);
    }

    public void setFlightForBooking(Flight flight){
        if(flightIconForBooking!=null){
            remove(flightIconForBooking);
            passengerName.setText(null);
            bookingActionButton.reset();
        }else{
            //Init Booking GUI
            passengerNameHint = new JLabel("Passagier Name oder Buchungsnummer eingeben");
            passengerNameHint.setBounds(350,350,300,27);
            add(passengerNameHint);

            passengerName = new JTextField();
            passengerName.setText(null);
            passengerName.setBounds(350,400,300,27);
            add(passengerName);

            bookingActionButton = new BookingButton();
            bookingActionButton.setBounds(650,400,100,27);
            add(bookingActionButton);
            
            tfName = new JTextArea();
            tfName.setBounds(800,400,200,180);
            tfName.setVisible(true);
            tfName.setEditable(false);
            tfName.setText("Keine Daten Vorhanden");
            tfName.setText(bookedNumbers(flight));
            add(tfName);
        }
        flightIconForBooking = new FlightIcon("SizedImages\\" + flight.getAirline()+".jpg",flight);
        flightIconForBooking.setBounds(0,300,300,400);
        add(flightIconForBooking);
        setTitle(flight.toString());
        invalidate();
        validate();
        repaint();
        GUI.getInstance().updateBookings(flight);

    }

    public String getBookingInput(){
        return passengerName.getText();
    }
    public Flight getBookingFlight(){
        return flightIconForBooking.getFlight();
    }
    
    public void clearTextfeld() {
    	passengerName.setText("");
    }
    
    public void updateBookings(Flight flight) {
    	tfName.setText(bookedNumbers(flight));
    }
    
    public String bookedNumbers(Flight flight) {
    	
    	HashMap<Integer, String> bookingsForFlights = BookingManager.getInstance().getBookings(flight);
    	
    	return MapToString(bookingsForFlights);
    }
    
    public String MapToString(HashMap<Integer, String> map) {
    	
    	if(map != null) {
	        StringBuilder mapAsString = new StringBuilder("{");
	        for (Integer key : map.keySet()) {
	            mapAsString.append(key + "=" + map.get(key) + " \n");
	        }
	        mapAsString.delete(mapAsString.length()-2, mapAsString.length()).append("}");
	        return mapAsString.toString();
    	}else {
    		return "Es sind keine Buchungen vorhanden";
    	}
    }
}
